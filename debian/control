Source: rdiff-backup
Section: utils
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Otto Kekäläinen <otto@debian.org>,
 Pablo Mestre Drake <pmdcuba@gmail.com>,
Build-Depends:
 asciidoctor,
 debhelper-compat (= 13),
 dh-python,
 librsync-dev,
 python3-all-dev,
 python3-pylibacl,
 python3-pyxattr,
 python3-setuptools,
 python3-setuptools-scm,
 python3-yaml,
Standards-Version: 4.7.0
Homepage: https://rdiff-backup.net/
Vcs-Git: https://salsa.debian.org/python-team/packages/rdiff-backup.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/rdiff-backup
Rules-Requires-Root: no

Package: rdiff-backup
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Recommends:
 python3-pylibacl,
 python3-pyxattr,
Description: remote incremental backup
 rdiff-backup backs up one directory to another, possibly over a network. The
 target directory ends up a copy of the source directory, but extra reverse
 diffs are stored in a special subdirectory of that target directory, so you can
 still recover files lost some time ago. The idea is to combine the best
 features of a mirror and an incremental backup. rdiff-backup also preserves
 subdirectories, hard links, dev files, permissions, uid/gid ownership,
 modification times, extended attributes, acls, and resource forks.
 .
 Also, rdiff-backup can operate in a bandwidth efficient manner over a pipe,
 like rsync. Thus you can use rdiff-backup and ssh to securely back a hard drive
 up to a remote location, and only the differences will be transmitted. Finally,
 rdiff-backup is easy to use and settings have sensible defaults.
